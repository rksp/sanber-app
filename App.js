import React from 'react';
import { StyleSheet, SafeAreaView, View } from 'react-native';

import QuizApp from './Tugas/Quiz3/index';

export default function App() {
  return (
    <SafeAreaView style={styles.container}>
      <View style={{ marginTop: 30, flex: 1 }}>
          <QuizApp />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
