import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class Skill extends React.Component {
    render() {
        const { skill: {skillName, categoryName, iconName, percentageProgress} } = this.props;
        return(
            <View style={styles.container}>
                <Icon name={iconName} style={styles.skillIcon} size={75}/>
                <View style={styles.skillDescription}>
                    <Text style={styles.skillName}>{skillName}</Text>
                    <Text style={styles.skillType}>{categoryName}</Text>
                    <Text style={styles.skillValue}>{percentageProgress}</Text>
                </View>
                <Icon name="chevron-right" size={75} color='#003366' />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'space-between',
        backgroundColor: '#B4E9FF',
        paddingVertical: 8,
        borderRadius: 8,
        elevation: 3,
        marginBottom: 14,
        flex: 1
    },
    skillIcon: {
        paddingHorizontal: 8,
        color: '#003366',
    },
    skillDescription: {
        flex: 1,
    },
    skillName: {
        fontSize: 20,
        color: '#003366',
        lineHeight: 24,
        fontWeight: 'bold'
    },
    skillType: {
        fontSize: 14,
        color: '#3EC6FF',
        fontWeight: 'bold'
    },
    skillValue: {
        fontSize: 48,
        color: '#fff'
    }
})