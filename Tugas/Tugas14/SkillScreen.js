import React from 'react';
import { View, StyleSheet, Image, Text, ScrollView, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Skill from './components/Skill';
import skills from './skillData.json';

export default class SkillScreen extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.logoContainer}>
                    <Image style={styles.logoImage} source={require('./images/sanber.png')} />
                    <Text style={styles.logoText}>PORTOFOLIO</Text>
                </View>
                <View style={styles.userContainer}>
                    <Icon name="account-circle" size={40} style={styles.userIcon} />
                    <View style={styles.userText}>
                        <Text style={styles.userGreeting}>Hai</Text>
                        <Text style={styles.userName}>Mukhlis Hanafi</Text>
                    </View>
                </View>
                <View style={styles.fullContainer}>
                    <Text style={styles.skillTitle}>SKILL</Text>

                    <View style={styles.tagContainer}>
                        <Text style={styles.tag}>Library / Framework</Text>
                        <Text style={styles.tag}>Bahasa Pemograman</Text>
                        <Text style={styles.tag}>Teknologi</Text>
                    </View>

                    <FlatList
                            data={skills.items}
                            renderItem={ ({item}) => <Skill skill={item} /> }
                            keyExtractor={item => item.id} />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 20,
        flex: 1
    },
    
    fullContainer: {
        flex: 1
    },
    logoContainer: {
        alignItems: 'flex-end',
        width: '100%',
    },
    logoText: {
        color: '#3EC6FF',
        fontWeight: 'bold',
        paddingHorizontal: 13
    },
    userContainer: {
        flexDirection: 'row',
        marginBottom: 16
    },
    userIcon:{
        color: '#3EC6FF'
    },
    userText: {
        paddingHorizontal: 10
    },  
    userGreeting: {
        fontSize: 12,
        color: '#003366'
    },
    userName: {
        fontSize: 16,
        color: '#003366' 
    },
    skillTitle: {
        fontSize: 36,
        borderBottomColor: '#3EC6FF',
        borderBottomWidth: 4,
        color: '#003366'
    },
    tagContainer: {
        flexDirection: 'row',
        marginVertical: 8,
        justifyContent: 'space-between'
    },
    tag: {
        padding: 8,
        backgroundColor: '#B4E9FF',
        borderRadius: 8,
        fontSize: 12,
        fontWeight: 'bold',
        color: '#003366'
    }
})