import React from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';


import { User, BackButton, List } from './components';

export class UserScreen extends React.Component {
    render() {
        const user = {
            name: 'James Bond',
            id: '007',
            badge: 'Frontend Specialist'
        }

        const data = [
            {
                name: 'Programming Languages',
                item: [
                    { name: 'Javascript', value: 96 },
                    { name: 'PHP', value: 72 },
                    { name: 'HTML', value: 99 },
                ]
            },
            {
                name: 'Framework / Libraries',
                item: [
                    { name: 'Angular', value: 96 },
                    { name: 'React', value: 70 }
                ]
            },
            {
                name: 'Technologies / Tools',
                item: [
                    { name: 'Figma', value: 30, sponsored: true },
                    { name: 'Git', value: 60 }
                ]
            }
        ]

        return (
            <ScrollView style={styles.container}>
                <View style={styles.backButtonContainer}>
                    <BackButton />
                </View>
                <View style={styles.mainContainer}>
                    <User user={user}/>
                    <View style={styles.listContainer} >
                        {data.map(i => <List items={i} key={i.name} />)}
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    backButtonContainer: {
        marginTop:20,
        paddingHorizontal: 20
    },
    mainContainer: {
        paddingHorizontal: 24,
        marginTop: 30
    },
    listContainer: {
        marginTop: 24
    }
})