import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome5';
import { Star } from './Star';

export class ListItem extends React.Component {
    render() {
        const { item: { name = 'Javascript', value = '96', sponsor = false } } = this.props;
        return (
            <View style={styles.container}>
                <View style={styles.itemIconContainer}>
                    <Icon name={iconNameMap[name]} size={42} />
                </View>
                <View style={styles.detailContainer}>
                    <Text style={styles.itemName}>{name}</Text>
                    <Star value={value} />
                </View>
            </View>
        )
    }
}

const iconNameMap = {
    'Javascript': 'js',
    'PHP': 'php',
    'HTML': 'html5',
    'Angular': 'angular',
    'React': 'react',
    'Figma': 'figma',
    'Git': 'git-square'
}
const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        marginVertical: 6
    },
    itemIconContainer: {
        minWidth: 62,
        justifyContent: 'center',
        alignItems: 'center'
    },
    itemName: {
        fontSize: 16,
        fontWeight: 'bold'
    }
})