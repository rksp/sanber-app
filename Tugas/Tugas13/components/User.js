import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons'

export class User extends React.Component {
    render() {
        const { user: { id, name, badge }} = this.props;
        return (
            <View style={styles.container}>
                <View style={styles.userImageContainer}>
                    <Icon name="md-person" size={60}></Icon>
                </View>
                <View style={styles.userDetailContainer}>
                    <Text style={styles.userID}>user #{id}</Text>
                    <Text style={styles.userName}>{name}</Text>
                    <Text style={styles.userBadge}>{badge}</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row'
    },
    userImageContainer: {
        borderWidth: 2,
        borderColor: '#000',
        padding: 8,
        justifyContent: 'center',
        alignItems: 'center',
        height: 66,
        width: 66
    },
    userDetailContainer: {
        flex: 1,
        paddingHorizontal: 12
    },
    userID: {
        fontSize: 12,
        fontWeight: '300'
    },
    userName: {
        fontSize: 24,
        fontWeight: 'bold'
    },
    userBadge: {
        fontSize: 16,
        fontWeight: 'normal'
    }
})