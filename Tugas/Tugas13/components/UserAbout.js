import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons'

export class UserAbout extends React.Component {
    render() {
        const { name} = this.props;
        return (
            <View style={styles.container}>
                <View style={styles.userImageContainer}>
                    <Icon name="md-person" size={60}></Icon>
                </View>
                <View style={styles.userDetailContainer}>
                    <Text style={styles.userTitle}>about me</Text>
                    <Text style={styles.userName}>{name}</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row'
    },
    userImageContainer: {
        borderWidth: 2,
        borderColor: '#000',
        padding: 8,
        justifyContent: 'center',
        alignItems: 'center',
        height: 66,
        width: 66
    },
    userDetailContainer: {
        flex: 1,
        paddingHorizontal: 12
    },
    userID: {
        fontSize: 12,
        fontWeight: '300'
    },
    userName: {
        fontSize: 24,
        fontWeight: 'bold'
    },
    userTitle: {
        fontSize: 16,
        fontWeight: 'normal'
    }
})