import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';

export class Star extends React.Component {
    render() {
        const { value } = this.props;
        const starValue = starCount(value)
        return (
            <View style={styles.container}>
                <View style={styles.starContainer}>
                    {Array.from({length: starValue.star}, _ => <Icon name="md-star" size={16} color='#F7DE00' />)}
                    {starValue.half ? <Icon name="md-star-half" size={16} color='#F7DE00' /> : null}
                </View>
                <Text style={styles.value}>{value}%</Text>
            </View>
        )
    }
}

const starCount = value => ({ star: Math.floor(value / 20), half: value % 20 >= 10})

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    starContainer: {
        flexDirection:'row'
    },
    value: {
        fontSize: 12,
        paddingHorizontal: 8,
        opacity: .5
    }
})