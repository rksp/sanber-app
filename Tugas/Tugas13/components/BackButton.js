import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

export class BackButton extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity>
                    <Icon name="md-arrow-back" size={25} />
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'flex-start',
        justifyContent: 'flex-start'
    },
})