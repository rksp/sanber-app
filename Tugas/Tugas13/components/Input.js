import React from 'react';
import { View, StyleSheet, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

export class Input extends React.Component {
     
    render() {
        const { type = 'username' } = this.props;
        
        return (
            <View style={styles.container}>
                <View style={styles.inputContainer}>
                    <Icon name={getLogo(type)} size={22} color="#999999" style={styles.inputIcon}/>
                    <TextInput style={styles.input} placeholder={getPlaceholder(type)} secureTextEntry={type === 'password'}></TextInput>
                </View>
            </View>
        )
    }
}

const getPlaceholder = type => {
    switch(type) {
        case 'username': return 'username';
        case 'password': return 'password';
        case 'email': return 'e-mail address'
    }
}

const getLogo = type => {
    switch(type) {
        case 'username': return 'md-person';
        case 'password': return 'md-lock';
        case 'email': return 'md-at'
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    inputContainer: {
        paddingVertical: 4,
        paddingHorizontal: 8,
        borderWidth: 2,
        borderColor: '#999999',
        flexDirection: 'row',
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center'
    },
    inputIcon: {
        marginRight: 16
    },
    input: {
        width: 220,
        fontSize: 16,
        color: '#999999'
    }
})