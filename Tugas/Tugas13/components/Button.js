import React from 'react';
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native';

export class Button extends React.Component {
    render() {
        const { text = 'log in', reverse = false } = this.props;
        return (
            <View style={styles.container}>
                <TouchableOpacity>
                    <Text style={reverse ? styles.buttonReverse : styles.button}>{text}</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
        width: 275,
        backgroundColor: '#4e4e4e',
        color: '#fff',
        textAlign: 'center',
        paddingVertical: 10,
        fontSize: 16,
        borderRadius:6
    },
    buttonReverse: {
        width: 275,
        backgroundColor: '#fff',
        color: '#4e4e4e',
        textAlign: 'center',
        paddingVertical: 10,
        fontSize: 16,
        borderRadius:6
    }
})