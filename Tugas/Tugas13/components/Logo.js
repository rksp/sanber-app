import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

export class Logo extends React.Component {
    render() {
        return(
            <View style={styles.logoContainer}>
                <View style={styles.code}>
                    <Text style={styles.codeText}>Code</Text>
                    <View style={styles.codeUnderline}></View>
                </View>
                <View style={styles.bench}>
                    <Text style={styles.benchText}>Bench</Text>
                    <View style={styles.benchUnderline}></View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    logoContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    codeText: {
        fontSize: 36,
        color: '#4e4e4e',
        fontWeight: 'normal'
    },
    benchText: {
        fontSize: 36,
        color: '#4e4e4e',
        fontWeight: 'bold',
    },
    codeUnderline: {
        backgroundColor: '#ffffff',
        borderColor: '#4e4e4e',
        borderWidth: 4,
        height: 12
    },
    benchUnderline: {
        backgroundColor: '#4e4e4e',
        borderColor: '#4e4e4e',
        borderWidth: 4,
        height: 12
    }
})