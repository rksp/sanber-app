import React from 'react';
import { View, StyleSheet } from 'react-native';

import { Logo, Input, Button } from './components'

export class LoginScreen extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.logoContainer}>
                    <Logo />
                </View>
                <View style={styles.usernameContainer}>
                    <Input />
                </View>
                <View style={styles.passwordContainer}>
                    <Input type="password" />
                </View>
                <View style={styles.loginButtonContainer}>
                    <Button />
                </View>
                <View style={styles.createAccountButtonContainer}>
                    <Button text="create an account" reverse={true} />
                </View>
                <View>
                    <Button text="about me" reverse={true} />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    logoContainer: {
        marginBottom: 60
    },
    usernameContainer: {
        marginBottom: 20
    },
    passwordContainer: {
        marginBottom: 60
    },
    loginButtonContainer: { 
        marginBottom: 20
    },
    createAccountButtonContainer: {
        marginBottom: 60
    }
})