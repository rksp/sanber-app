import React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createDrawerNavigator } from '@react-navigation/drawer'

import { LoginScreen, RegisterScreen, AboutScreen, ProjectScreen, AddScreen, Splash, UserScreen } from './screens';
import { AuthContext } from './contexts';

const AuthStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const SkillStack = createStackNavigator();
const ProjectStack = createStackNavigator();
const AddStack = createStackNavigator();
const AboutStack = createStackNavigator();
const RootStack = createStackNavigator();

const AuthStackScreen = () => (
    <AuthStack.Navigator>
        <AuthStack.Screen name="Login" component={LoginScreen} options={{ 
            title: 'aaa',
            headerShown: false,
        }} />
        <AuthStack.Screen name="Register" component={RegisterScreen} options={{ 
            title: '',
            headerTransparent: true
        }} />
        <AuthStack.Screen name="About" component={AboutScreen} options={{ 
            title: '',
            headerTransparent: true
        }} />
    </AuthStack.Navigator>
)

const SkillStackScreen = () => (
    <SkillStack.Navigator>
        <SkillStack.Screen name="Skill" component={UserScreen} options={{
            title: null,
            headerTransparent: true
        }} />
    </SkillStack.Navigator>
)

const ProjectStackScreen = () => (
    <ProjectStack.Navigator>
        <ProjectStack.Screen name="Project" component={ProjectScreen} />
    </ProjectStack.Navigator>
)

const AddStackScreen = () => (
    <AddStack.Navigator>
        <AddStack.Screen name="Add" component={AddScreen} />
    </AddStack.Navigator>
)

const AboutStackScreen = () =>(
    <AboutStack.Navigator>
        <AboutStack.Screen name="About" component={AboutScreen} options={{ 
            title: '',
            headerTransparent: true
        }}  />
    </AboutStack.Navigator>
)

const TabScreen = () => (
    <Tabs.Navigator>
        <Tabs.Screen name="Skill" component={SkillStackScreen} />
        <Tabs.Screen name="Project" component={ProjectStackScreen} />
        <Tabs.Screen name="Add" component={AddStackScreen} />
    </Tabs.Navigator>
)

const DrawerScreen = () => (
    <Drawer.Navigator>
        <Drawer.Screen  name="Skill" component={TabScreen} />
        <Drawer.Screen  name="About" component={AboutStackScreen} />
    </Drawer.Navigator>
)

const RootStackScreen = ({ userToken }) => (
    <RootStack.Navigator headerMode="none">
        {userToken 
            ? (<RootStack.Screen name="App" component={DrawerScreen} options={{
                animationEnabled: false
            }} />)
            : (<RootStack.Screen name="Auth" component={AuthStackScreen} options={{
                animationEnabled: false
            }} />)}
    </RootStack.Navigator>
)

export const NavigationApp = () => {
    const [isLoading, setLoading] = React.useState(true);
    const [userToken, setUserToken] = React.useState(null);

    const authContext = React.useMemo(() => {
        return {
            signIn: () => {
                setLoading(false);
                setUserToken('xxx')
            },
            signUp: () => {
                setLoading(false)
                setUserToken('xxx')
            },
            signOut: () => {
                setLoading(false)
                setUserToken(null)
            }
        }
    })

    React.useEffect(() => {
        setTimeout(() => {
            setLoading(false)
        }, 1000)
    })

    if(isLoading) {
        return (<Splash />)
    }

    return (
        <AuthContext.Provider value={authContext}>
            <NavigationContainer>
                <RootStackScreen userToken={userToken} />
            </NavigationContainer>
        </AuthContext.Provider>
    )
}