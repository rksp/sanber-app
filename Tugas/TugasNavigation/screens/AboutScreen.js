import React from 'react';
import { View, StyleSheet, ScrollView, Text } from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome5';
import { UserAbout } from './components';

export const AboutScreen = () => {
        return (
            <ScrollView style={styles.container}>
                <View style={styles.mainContainer}>
                    <UserAbout name="Riki Syahputra"/>
                    <Text style={styles.description} multiline="true">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris nec ornare urna. Integer vehicula lacus vel eleifend finibus. Quisque maximus, nisl et tincidunt venenatis, mi tellus mattis urna, vel porta nibh lectus eget metus. Quisque pulvinar semper elit at pretium. Vivamus congue feugiat dapibus. 
                    </Text>
                    
                    <View style={styles.socialMedia}>
                        <Icon style={styles.socialIcon} name="facebook" />
                        <Text>facebook.com/someone</Text>
                    </View>
                    
                    <View style={styles.socialMedia}>
                        <Icon style={styles.socialIcon} name="twitter" />
                        <Text>twitter.com/someone</Text>
                    </View>
                    
                    <View style={styles.socialMedia}>
                        <Icon style={styles.socialIcon} name="instagram" />
                        <Text>instagram.com/someone</Text>
                    </View>
                    
                    <View style={styles.socialMedia}>
                        <Icon style={styles.socialIcon} name="github" />
                        <Text>github.com/someone</Text>
                    </View>
                </View>
            </ScrollView>
        )
    
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 30
    },
    backButtonContainer: {
        marginTop:20,
        paddingHorizontal: 20
    },
    mainContainer: {
        paddingHorizontal: 24,
        marginTop: 30
    },
    listContainer: {
        marginTop: 24
    },
    socialMedia: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        opacity: .7
    },
    socialIcon: {
        fontSize: 30,
        padding: 8
    },
    description: {
        marginVertical: 24,
        lineHeight: 20,
        opacity: .7
    }
})