import React from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';

import { User, List } from './components';

export const Skill = () => {
    const user = {
        name: 'James Bond',
        id: '007',
        badge: 'Frontend Specialist'
    }

    const data = [
        {
            id: '001',
            name: 'Programming Languages',
            item: [
                { name: 'Javascript', value: 96 },
                { name: 'PHP', value: 72 },
                { name: 'HTML', value: 99 },
            ]
        },
        {
            id: '002',
            name: 'Framework / Libraries',
            item: [
                { name: 'Angular', value: 96 },
                { name: 'React', value: 70 }
            ]
        },
        {   
            id: '003',
            name: 'Technologies / Tools',
            item: [
                { name: 'Figma', value: 30, sponsored: true },
                { name: 'Git', value: 60 }
            ]
        }
    ]

    return (
        <ScrollView style={styles.container}>
            <View style={styles.mainContainer}>
                <User user={user}/>
                <View style={styles.listContainer} >
                    {data.map(i => <List items={i} key={i.id} />)}
                </View>
            </View>
        </ScrollView>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    backButtonContainer: {
        marginTop:20,
        paddingHorizontal: 20
    },
    mainContainer: {
        paddingHorizontal: 24,
        marginTop: 30
    },
    listContainer: {
        marginTop: 24
    }
})