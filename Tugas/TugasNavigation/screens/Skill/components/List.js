import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

import { ListItem } from './List-item';

export class List extends React.Component {
    render() {

        const { items: {item, name} } = this.props;

        return(
            <View style={styles.container}>
                <Text style={styles.itemsName}>{name}</Text>
                {item.map(item => <ListItem item={item} key={item.name} style={styles.itemContainer} />)}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        marginVertical: 10
    },
    itemsName: {
        opacity: .7,
        fontSize: 14
    },
    itemContainer: {
        padding: 10
    }
})