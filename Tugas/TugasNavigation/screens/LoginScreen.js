import React from 'react';
import { View, StyleSheet } from 'react-native';

import { Logo, Input, Button } from './components';
import { AuthContext } from '../contexts';

export const LoginScreen = ({ navigation }) => {

    const { signIn } = React.useContext(AuthContext);

    return (
        <View style={styles.container}>
            <View style={styles.logoContainer}>
                <Logo />
            </View>
            <View style={styles.usernameContainer}>
                <Input />
            </View>
            <View style={styles.passwordContainer}>
                <Input type="password" />
            </View>
            <View style={styles.loginButtonContainer}>
                <Button onPress={() => signIn()} />
            </View>
            <View style={styles.createAccountButtonContainer}>
                <Button text="create an account" reverse={true} onPress={() => navigation.push('Register')} />
            </View>
            <View>
                <Button text="about me" reverse={true} onPress={() => navigation.push('About')}  />
            </View>
        </View>
    )
    
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    logoContainer: {
        marginBottom: 60
    },
    usernameContainer: {
        marginBottom: 20
    },
    passwordContainer: {
        marginBottom: 60
    },
    loginButtonContainer: { 
        marginBottom: 20
    },
    createAccountButtonContainer: {
        marginBottom: 60
    }
})