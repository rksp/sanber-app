import React from 'react';
import { View, StyleSheet, Text, Button } from 'react-native';

import { AuthContext } from '../contexts';

export const ProjectScreen = () => {
    
    const { signOut } = React.useContext(AuthContext);

    return (
        <View style={styles.container}>
            <Text>Project Screen</Text>
            <Button title='Log Out' onPress={() => signOut()} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})