import React from 'react';
import { View, StyleSheet } from 'react-native';

import { Logo, Input, Button } from './components'

export const RegisterScreen = () => {
    return (
        <View style={styles.container}>
            <View style={styles.innerContainer}>
                <View style={styles.logoContainer}>
                    <Logo />
                </View>
                <View style={styles.emailContainer}>
                    <Input type="email" />
                </View>
                <View style={styles.usernameContainer}>
                    <Input />
                </View>
                <View style={styles.passwordContainer}>
                    <Input type="password" />
                </View>
                <View style={styles.registerButtonContainer}>
                    <Button text="register" />
                </View>
            </View>
        </View>
    )
    
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    backButtonContainer: {
        marginTop:20,
        paddingHorizontal: 20
    },
    innerContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    logoContainer: {
        marginBottom: 60,
        marginTop: -20
    },
    emailContainer: {
        marginBottom: 20
    },
    usernameContainer: {
        marginBottom: 20
    },
    passwordContainer: {
        marginBottom: 60
    },
    registerButtonContainer: { 
        marginBottom: 60
    }
})